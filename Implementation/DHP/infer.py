import numpy as np
import math as mth
import scipy.stats as sst
import matplotlib as mpl
import pickle as pkl
import re

mpl.use("qt4agg")
import matplotlib.pyplot as plt

# Functions


def exp_list(ls):
    ls2 = []
    for i in range(len(ls)):
        ls2.append(mth.exp(ls[i]))
    return ls2


def pi_of(eta, index):
    return mth.exp(eta[index]) / np.sum(exp_list(eta))


def mini(a, b):
    if a < b:
        return a
    else:
        return b


def init_vars(k, n, v, w, e, u, cumulative_phi_s, beta_s, eta_s, pi_of_s, cumulative_eta_s, alpha_s, mu_s, As):
    triggers = []
    a = 100/9
    _b = 10/9
    for i in range(u):
        alpha_s[i] = sst.dirichlet.rvs(np.ones(k) * 2, 1)[0]
    for i in range(e):
        eta_s[i] = sst.dirichlet.rvs(np.ones(k) * 2, 1)[0]
        cumulative_eta_s[i] = eta_s[i] + (i > 0) * cumulative_eta_s[i - 1]
        for j in range(n):
            cumulative_phi_s[i][j] = sst.dirichlet.rvs(np.ones(k) * 2, 1)[0] + (j > 0) * cumulative_phi_s[i][j - 1]
        for j in range(k):
            pi_of_s[i][j] = pi_of(eta_s[i], j)
    for i in range(k):
        ax = np.random.choice(v, w)
        for j in range(w):
            betas[i][ax[j]] = 1 / w
    r_s = []
    # print("E: " + str(e))
    for i in range(e):
        r_s.append(np.ones(i + 1) * -float("inf"))
        x = np.random.choice(i + 1)
        r_s[len(r_s) - 1][x] = 0
        triggers.append(x)
    for i in range(u):
        mu_s.append(sst.gamma.rvs(a, 0, (1/_b), 1))
    number_of_adj = int(u / 10)
    for i in range(0, u):
        inds = np.random.choice(u, number_of_adj, replace=False, p=None)
        for j in range(0, number_of_adj):
            As[i][inds[j]] = sst.gamma.rvs(a, 0, 1 / b, 1)
    return r_s, triggers


# delay distributions, currently it is N(1, 1)
def log_f_delta(delta, mu, sigma):
    return -(0.5 * (mth.log(2) + mth.log(mth.pi)) + mth.log(sigma)) + (-0.5 * (delta - 1) ** 2)


def my_max(lst, last_index):
    maxim = -float("inf")
    maxim_i = -1
    for i in range(last_index + 1):
        if lst[i] > maxim:
            maxim = lst[i]
            maxim_i = i
    return maxim_i, maxim


# TODO : Real Data
# # Generating Documents
file = open("top100.csv", "r")
data = file.read()
real_events = data.split('\n')
usrId = 0
userConv = dict()
usr_counter = 0
phis_sizes = []
E = 0
n = 0
N = 0
events = []
it = "124.0"
max_num_of_events = 0
p = re.compile('^[0-9]+[:][0-9]+$')
# print(p.match(it))
for event in real_events:
    if max_num_of_events < 500:
        event_data = event.split('\t')
        if len(event_data) < 5:
            continue
        time = float(event_data[0])
        user = int(event_data[5])
        usr_id = userConv.get(user, usr_counter)
        if usr_id == usr_counter:
            usr_counter += 1
        userConv[user] = usr_id
        l = list()
        n = 0
        for item in event_data:
            if p.match(item) is None:
                continue
            _is = item.split(':')
            count = int(_is[1])
            for i in range(count):
                l.append(int(_is[0]))
                n += 1
        if n > N:
            N = n
        events.append((time, usr_counter - 1, l))
        # print("EV: " + str(len(events[len(events) - 1][2])) + "lens" + str(len(events)))
        E += 1
        max_num_of_events += 1
dtype = [("time", float), ("user", int), ("document", list)]
events = np.array(events, dtype=dtype)
events = np.sort(events, order="time")
for i in range(len(events) - 1):
    events[i + 1][0] -= events[0][0]
events[0][0] = 0
for i in range(len(events)):
    phis_sizes.append(len(events[i][2]))
# print(events)
# print(E)
# ## This is syntethic data generator
# # TODO : Generating synthetic data
# lambdas = [mus]  # intensities
# ts = [0]  # t_k s
# T = 0
# num = 1
# while num <= D:
#     flag = False
#     min_t = float('inf')
#     min_f = float('inf')
#     min_t_ind = -1
#     min_f_ind = -1
#     i = len(lambdas)
#     for j in range(0, U):
#         u_1 = sst.uniform.rvs(0, 1, 1)
#         u_2 = sst.uniform.rvs(0, 1, 1)
#         s = (-1/lambdas[i - 1][j]) * mth.log(u_1)
#
#         lambda_s = (lambdas[i - 1][j] - mus[j]) * f_delta(s) + mus[j]
#         if u_2 > lambda_s / lambdas[i - 1][j]:
#             if min_f > s:
#                 min_f = s
#                 min_f_ind = j
#         else:
#             if min_t > s:
#                 min_t = s
#                 min_t_ind = j
#             flag = True
#     adder = 0
#     if flag:
#         [ind, sk] = [min_t_ind, min_t]
#         ts.append(ts[len(ts) - 1] + sk)
#         events.append([ts[len(ts) - 1], ind, []])
#         adder = 1
#     else:
#         [ind, sk] = [min_f_ind, min_f]
#         adder = 0
#     T += sk
#     newlambdas = []
#     for j in range(0, U):
#         newlambdas.append((lambdas[i - 1][j] - mus[j]) * f_delta(sk) + mus[j] + A[ind][j] * adder)
#     lambdas.append(newlambdas)
#     num += 1

### Parametera
# Dimensions
U = len(userConv)
K = 10  # number of topics
W = 10
V = 100000  # number of all words

# variational parameters
cum_phis = np.zeros((E, N, K))  # for each document cumulative sum of phis of different words through different topics
# print(str(E) + "," + str(N) + "," + str(K) + "pfff: " + str(cum_phis))
z_s = np.zeros((E, N))
rs = []

# main parameters
betas = np.zeros((K, V))
sigma = 1
alphas = np.zeros((U, K))
T = 10  # whole time of events

# latent variables
etas = np.zeros((E, K))

# additional parameters
etas_hessian = np.zeros((K, K))
pi_ofs = np.zeros((E, K))
cum_etas = np.zeros((E, K))
gamma = 0.1  # parameter of newton gradient method used for posterior estimation of q(eta)
# f_delta(t) is Normal(mu, sigma * I)
mu = 1
mus = []
b = 1
A = np.zeros((U, U))
alpha = 2
trigger = []

# initialization
[rs, trigger] = init_vars(K, N, V, W, E, U, cum_phis, betas, etas, pi_ofs, cum_etas, alphas, mus, A)
# print("pfff: " + str(cum_phis))

I_K = np.identity(K)

# Generating betas
for i in range(K):
    betas[i] = sst.dirichlet.rvs(alpha * np.ones(V))

# # Generating Triggerers
# endogenous = []
# for i in range(0, E):
#     u = events[i][1]
#     t = events[i][0]
#     sums = mus[u]
#     for k in range(0, i):
#         sums += A[events[k][1]][u] * f_delta(t - events[k][0])
#     u_1 = sst.uniform.rvs(0, sums, 1)
#     sums = 0
#     flag = 0
#     for k in range(0, i):
#         sums += A[events[k][1]][u] * f_delta(t - events[k][0])
#         if u_1[0] <= sums:
#             flag = 1
#             trigger.append(k)
#             break
#     if flag == 0:
#         trigger.append(i)
#         endogenous.append(i)

# # Generating etas
# for e in range(E):
#     if trigger[e] == e:
#         etas[e] = sst.multivariate_normal.rvs(alphas[events[e][1]], sigma ** 2 * I_K, 1)
#     else:
#         etas[e] = sst.multivariate_normal.rvs(etas[trigger[e]], sigma ** 2 * I_K, 1)
#     for i in range(K):
#         pi_ofs[e][i] = pi_of(etas[e], i)

# # Generating phis_sizes & words
# for i in range(D):
#     phis_sizes[i] = mini(sst.poisson.rvs(N_mean), N)
#     for j in range(int(phis_sizes[i])):
#         z_s[i][j] = (np.random.choice(K, 1, replace=True, p=pi_ofs[i]))[0]
#         events[i][2].append((np.random.choice(V, 1, replace=True, p=betas[int(z_s[i][j])]))[0])

# TODO (At the end of each iteration):
# Update pi_ofs
# Update cum_phis
# Update phis_sizes
# Update cumulative eta

it = 5  # number of iterations
for itt in range(it):
    # updating variational parameters
    for t in range(E):
        r_in_eta = 0
        for i in range(t):
            r_in_eta += mth.exp(rs[t][i]) * etas[i]
        # calculating gradient of f(eta)
        # print("phhhh: " + str(cum_phis))
        sum_of_all_phis = np.sum(cum_phis[t][phis_sizes[t] - 1])
        # print("T = " + str(t))
        # print("etas = " + str(etas))
        # print("events = " + str(events[t][1]))
        # print(U)
        # print("alphas = " + str(len(alphas)))
        # print("phis = " + str(phis_sizes))
        # print("p_ofs = " + str(pi_ofs))
        # print("rs = " + str(rs))
        grad = -1/(sigma**2) * (etas[t] - mth.exp(rs[t][t]) * alphas[events[t][1]] - r_in_eta) + cum_phis[t][phis_sizes[t] - 1] \
               - pi_ofs[t] * sum_of_all_phis
        # calculating hessian of f(eta)
        for i in range(K):
            for j in range(K):
                etas_hessian[i][j] = (pi_ofs[t][i] * pi_ofs[t][j] - (i == j) * pi_ofs[t][i]) * \
                                     sum_of_all_phis - (i == j) / (sigma ** 2)
        #### updating variational parameters
        # updating etas
        etas[t] += np.linalg.solve(etas_hessian, -grad)
        # updating pi_ofs
        for j in range(K):
            pi_ofs[i][j] = pi_of(etas[i], j)
        # updating rs
        rs[t][t] = -1 - (1/(sigma**2)) * np.linalg.norm(etas[t] - alphas[events[t][1]]) ** 2
        for i in range(t):
            # print(events[t][0] - events[i][0])
            rs[t][i] = -1 - (1/(sigma**2)) * np.linalg.norm(etas[t] - etas[i]) ** 2 + \
                               log_f_delta(events[t][0] - events[i][0], 100, 10)
        # updating phis
        for i in range(int(phis_sizes[t])):
            # print("I = " + str(i))
            # print("T = " + str(t))
            # print("event" + str(len(events[t][2])))
            # print("phis: " + str(phis_sizes[t]))
            cum_phis[t][i] = (i > 0) * cum_phis[t][i - 1] + np.multiply(pi_ofs[t], (np.transpose(betas))[events[t][2][i]])
        # Generating Triggerers
        trigger[t] = (my_max(rs[t], t))[0] - 1

    #### updating main parameters
    # updating betas
    betas = np.zeros((K, V))
    for k in range(K):
        for e in range(E):
            for n in range(int(phis_sizes[e])):
                betas[k][events[e][2][n]] += (cum_phis[e][n] - (n > 0) * cum_phis[e][n - 1])[k]

    # updating alphas
    alphas = np.zeros(U)
    for i in range(E):
        alphas[events[i][1]] += mth.exp(rs[i][i])

    # updating A & mu
    A = np.zeros((U, U))
    l = np.zeros(U)
    for i in range(E):
        for j in range(i):
            A[events[j][1]][events[i][1]] += mth.exp(rs[i][j])
        l[events[i][1]] += 1
    for i in range(U):
        if l[i] != 0:
            A[i] /= l[i]
    for i in range(E):
        mus[events[i][1]] += mth.exp(rs[i][i]) / T

# saving result
f = open("./A.pk", "wb")
pkl.dump(A, f)
f.close()

f = open("./mus.pk", "wb")
pkl.dump(mus, f)
f.close()

f = open("./etas.pk", "wb")
pkl.dump(etas, f)
f.close()

f = open("./pi_ofs.pk", "wb")
pkl.dump(pi_ofs, f)
f.close()

f = open("./rs.pk", "wb")
pkl.dump(rs, f)
f.close()

f = open("./cum_phis.pk", "wb")
phis = np.zeros((E, N, K))
# converting phis to original state
for i in range(E):
    for j in range(N):
        phis[i][j] = cum_phis[i][j] - (j > 0) * cum_phis[i][j - 1]
pkl.dump(cum_phis, f)
f.close()

f = open("./betas.pk", "wb")
pkl.dump(betas, f)
f.close()

f = open("./alphas.pk", "wb")
pkl.dump(alphas, f)
f.close()










