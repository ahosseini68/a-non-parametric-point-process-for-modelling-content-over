__author__ = 'soheil'
import numpy as np
from numpy import random as random
from collections import deque
from scipy import special as special

from TopicModel.topic_model import TopicModel


class Particle:
    def __init__(self, a, b, c, d, gamma, zeta, epsilon, kernel_mean, kernel_var, topic_decreasing_coefficient,
                 user_num=10000, number_of_samples=1000):
        self.particle_weight = 1

        self.user_num = user_num
        self.number_of_topics = 0
        self.number_of_docs = 0

        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.gamma = gamma
        self.zeta = zeta
        self.kessi = topic_decreasing_coefficient
        self.number_of_samples = number_of_samples

        self.kernel_mean = kernel_mean
        self.kernel_var = kernel_var
        self.threshold = np.log(epsilon) / np.max([kernel_mean - 2 * kernel_var, 0.01])

        self.counts = np.zeros(shape=(user_num, user_num), dtype=int)
        self.topics = list()

        self.events = dict()
        self.events_of_users = dict()

        self.first_active_event = 0

        self.sum_of_time_diff = dict()

        self.m_ku = dict()
        self.m_k = dict()
        self.m_u0 = np.zeros(self.user_num)
        self.m_0 = 0
        self.last_update_time = 0

    def update(self, time, user, document):
        self.deque_old_events(time)

        self.number_of_docs += 1
        tmp_topic = TopicModel()

        self.compute_topic_weights(time)

        omega_u = self.compute_sum_omega_u(time)
        number_of_active_docs = self.number_of_docs - self.first_active_event

        s_log_prob = np.zeros(number_of_active_docs)
        z_log_prob = np.zeros(shape=(self.number_of_topics + 1, 2))
        for s in range(self.first_active_event, self.number_of_docs):
            print(s)
            if s == self.number_of_docs - 1:
                s_log_prob[s - self.first_active_event] = np.log(self.c + self.counts[user][user]) - np.log(
                    self.d + time)
                print('here: {0}'.format(s_log_prob[s - self.first_active_event]))
            else:
                z_s = self.events[s]["topic"]
                t_s = self.events[s]["time"]
                s_log_prob[s - self.first_active_event] = -1 * self.topics[z_s].beta * (time - t_s) + \
                                                          np.log(self.a + self.counts[self.events[s]["user"]][user]) - \
                                                          np.log(
                                                              self.b + omega_u.get((self.events[s]['user'], user), 0))

        for z in range(self.number_of_topics + 1):
            print('z: {0}'.format(z))
            if z < self.number_of_topics:
                z_log_prob[z][0] += self.topics[z].log_likelihood(document)
                z_log_prob[z][0] += (self.m_ku[z][user]) - np.log(np.exp(self.m_u0[user]) + self.gamma)
            else:
                z_log_prob[z][0] = -1 * np.inf
            if z < self.number_of_topics:
                z_log_prob[z][1] += self.topics[z].log_likelihood(document)
                z_log_prob[z][1] += np.log(self.gamma) + (self.m_k[z]) - np.log(
                    np.exp(self.m_0) + self.zeta) - np.log(
                    np.exp(self.m_u0[user]) + self.gamma)
            else:
                z_log_prob[z][1] += tmp_topic.log_likelihood(document)
                z_log_prob[z][1] += np.log(self.zeta) + np.log(self.gamma) - np.log(
                    self.zeta + np.exp(self.m_0)) - np.log(np.exp(self.m_u0[user]) + self.gamma)
        log_prob = np.zeros(number_of_active_docs + 2 * self.number_of_topics)
        print('logprob_shape: {0}\tnum_of_active_doc: {1}'.format(log_prob.shape, number_of_active_docs))
        for i in range(number_of_active_docs - 1):
            log_prob[i] = s_log_prob[i] + self.topics[self.events[i + self.first_active_event]["topic"]].log_likelihood(
                document)

        for k in range(self.number_of_topics):
            log_prob[k + number_of_active_docs - 1] = s_log_prob[-1] + z_log_prob[k][0]
            log_prob[k + self.number_of_topics + number_of_active_docs - 1] = s_log_prob[-1] + z_log_prob[k][1]
        log_prob[-1] = s_log_prob[-1] + z_log_prob[-1][1]
        print(log_prob)
        print(s_log_prob[-1] + z_log_prob[-1][1])
        prob = np.exp(log_prob - np.max(log_prob))
        prob = prob / np.sum(prob)
        index = random.choice(len(prob), size=1, p=prob)[0]
        r = 0
        if index < number_of_active_docs - 1:
            s = index + self.first_active_event
            z = self.events[s]["topic"]
        else:
            s = self.number_of_docs - 1
            if index < number_of_active_docs + self.number_of_topics - 1:
                z = index - (number_of_active_docs - 1)
                r = 0
            else:
                z = index - (number_of_active_docs - 1) - self.number_of_topics
                r = 1

        new_event = dict()
        new_event['time'] = time
        new_event['user'] = user
        new_event['document'] = document
        new_event['topic'] = z
        new_event['parent'] = s
        new_event['document_number'] = self.number_of_docs - 1
        new_event['parent_user'] = s
        if s != user:
            new_event['parent_user'] = self.events[s]['user']

        temp_deque = self.events_of_users.get(user, deque())
        temp_deque.append(new_event)
        self.events_of_users[user] = temp_deque
        self.events[self.number_of_docs - 1] = new_event

        # Updating topics
        if z == self.number_of_topics:
            self.number_of_topics += 1
            self.topics.append(tmp_topic)
            self.m_ku[z] = np.zeros(self.user_num)
            self.m_k[z] = 0
            self.sum_of_time_diff[z] = 0

        self.update_weight_of_topic(z, user, r)
        self.topics[z].update(document)

        # Updating the counts
        self.counts[self.events[s]['user']][user] += 1

        # Updating the sum_of_time_diffs
        if s != self.number_of_docs - 1:
            self.sum_of_time_diff[z] += time - self.events[s]['time']

        # Updating the weight of the particle
        self.update_weight(document, time, user)

        # Updating the kernel
        self.update_beta(z, time)

        return s, z

    def update_weight(self, document, time, user):
        sum_omega_u = self.compute_sum_omega_u(time)

        s1 = 0
        for z in range(self.number_of_topics):
            s1 += np.exp(self.topics[z].log_likelihood(document)) * (
                self.m_ku[z] + self.gamma * self.m_k[z] / (self.m_0 + self.zeta)) / (self.m_u0 + self.gamma) * (
                      self.c + self.counts[-1][-1]) / (self.d + time)

        for s in range(self.number_of_docs - 1):
            s1 += np.exp(self.topics[self.events[s]['topic']].log_likelihood(document)) * (
                self.a + self.counts[self.events[s]["user"]][user]) / (
                      self.b + sum_omega_u.get((self.events[s]["user"], user), 0))

        self.particle_weight *= s1

    def deque_old_events(self, time):
        users = self.events_of_users.keys()
        for user in users:
            d = self.events_of_users[user]
            while len(d) != 0 and time - d[0]['time'] < self.threshold:
                x = d.popleft()
            if len(d) != 0 and self.first_active_event < d[0]['document_number']:
                self.first_active_event = d[0]['document_number']
            self.events_of_users[user] = d

    def compute_topic_weights(self, time):
        for topic in self.m_ku.keys():
            self.m_ku[topic] -= self.kessi * (time - self.last_update_time)
            self.m_k[topic] -= self.kessi * (time - self.last_update_time)
        self.m_u0 -= self.kessi * (time - self.last_update_time)
        self.m_0 -= self.kessi * (time - self.last_update_time)
        self.last_update_time = time

    def update_weight_of_topic(self, z, user, r):  # m_u_k(t)
        self.m_ku[z][user] = np.log(np.exp(self.m_ku[z][user]) + 1)
        self.m_u0[user] = np.log(np.exp(self.m_u0[user]) + 1)
        if r == 1:
            self.m_k[z] = np.log(np.exp(self.m_k[z]) + 1)
            self.m_0 = np.log(np.exp(self.m_0) + 1)

    def compute_sum_omega_u(self, time):
        sum_omega_u = dict()

        for user in self.events_of_users.keys():
            for event in self.events_of_users[user]:
                triggering_event = self.events[event["parent"]]
                user_s = triggering_event["user"]
                k_s = triggering_event["topic"]
                beta_k = self.topics[k_s].beta
                t_s = triggering_event["time"]
                sum_omega_u[(user_s, user)] = sum_omega_u.get((user_s, user), 0) + 1 / beta_k * (
                    1 - np.exp(-beta_k * (time - t_s)))
        return sum_omega_u

    def update_beta(self, k, time):
        scale = self.kernel_var / self.kernel_mean
        shape = self.kernel_mean / scale
        beta_sample = random.gamma(shape=shape, scale=scale, size=self.number_of_samples)
        weight = np.zeros(self.number_of_samples)
        old_beta = self.topics[k].beta
        for i in range(self.number_of_samples):
            weight[i] = self.compute_log_likelihood(k, beta_sample[i], time)
        self.topics[k].beta = old_beta
        weight /= np.sum(weight)
        beta_sample.dot(weight)
        beta = np.sum(beta_sample)
        self.topics[k].update_kernel(beta)
        return beta

    def compute_log_likelihood(self, k, beta, time):
        result = -1 * beta * self.sum_of_time_diff[k]
        omega_u_dict = self.compute_sum_omega_u(time)
        omega_u = np.array(list(omega_u_dict.values()))
        result -= np.sum(np.log(self.b + np.sum(omega_u)))
        return result
