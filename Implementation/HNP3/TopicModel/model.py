__author__ = 'soheil'

from TopicModel.particle import Particle
import numpy as np
from numpy import random as random


class NP3:
    def __init__(self, a, b, c, d, gamma, zeta, epsilon, kernel_var,kernel_mean, topic_decreasing_coefficient, number_of_user=500, number_of_particles=200):
        self.particles = [Particle(a, b, c, d, gamma, zeta, epsilon, kernel_mean, kernel_var, topic_decreasing_coefficient) for i in range(number_of_particles)]
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.gamma = gamma
        self.zeta = zeta
        self.number_of_doc = 0
        self.thresh = 10
        self.number_of_user = number_of_user

    def add_event(self, time, user, document):
        for particle in self.particles:
            particle.update(time, user, document)
        self.resample_particles()

    def resample_particles(self):
        weights = list()
        for particle in self.particles:
            weights.append(particle.particle_weight)
        weights = np.array(weights)
        weights = weights / np.sum(weights)
        if self.thresh > (1 / np.sum(weights ** 2)):
            indices = random.choice(len(weights), size=len(weights), p=weights)
            temp_particles = self.particles
            for i in range(len(self.particles)):
                self.particles[i] = temp_particles[indices[i]]

    def alpha_expectation(self, time):
        alpha = np.ndarray((self.number_of_user, self.number_of_user))
        for j in range(self.number_of_user):
            for i in range(self.number_of_user):
                alpha[i][j] = 0
                weight_sum = 0
                for particle in self.particles:
                    temp = particle.user_event_count[i]
                    x = temp[2] + (-self.gamma * (time - temp[1]))
                    x = np.exp(x)
                    alpha[i][j] += particle.particle_weight * (self.c + particle.counts[i][j]) / \
                                   (self.d + self.gamma * (temp[0] - x))
                    weight_sum += particle.particle_weight
                alpha[i][j] /= weight_sum
        return alpha

        # def phi_posterior(self):
        




