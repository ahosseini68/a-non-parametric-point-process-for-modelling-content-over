__author__ = 'soheil'
import numpy as np
from numpy import random as random


class TopicModel():
    def __init__(self, n=100000, kernel_num=1, number_of_samples=1000):
        self.eta = np.ones(shape=n)
        self.size = n
        self.beta = 1
        self.number_of_samples = number_of_samples

    def log_likelihood(self, document):
        likelihood = 0
        eta_sum = np.sum(self.eta)
        for l in range(document['length']):
            likelihood -= np.log(eta_sum + l)

        for w in document['words'].keys():
            for l in range(document['words'][w]):
                likelihood += np.log(self.eta[w] + l)
        return likelihood

    def update(self, document):
        for word_idx in document['words'].keys():
            self.eta[word_idx] += document['words'][word_idx]

    def update_kernel(self, beta):
        self.beta = beta








