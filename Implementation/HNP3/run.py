__author__ = 'soheil'

import line_profiler
from TopicModel.model import NP3
import numpy as np
import timeit

data_path = '/home/soheil/Desktop/uai/a-non-parametric-point-process-for-modelling-content-over/Implementation/HNP3/'
data_file = open(data_path+'../data/1.csv','r')
alpha_file = open('alpha_exp.csv','w')
my_model = NP3(a=0.16,b=16,c=16,d=1.6,gamma=0.1,zeta=0.01,epsilon=16e-6,kernel_mean=2.5e-4,kernel_var=0.5e-4,topic_decreasing_coefficient=2.5e-4,number_of_user=100,number_of_particles=8)
loop_counter = 0
source_id_map = dict()
user_counter = 0
for line in data_file:
    loop_counter += 1
#    print(loop_counter)
    splited = line.split('\t')
    time = int(float(splited[0]))
    event_id = int(splited[3])
    length = int(splited[7])
    user = source_id_map.get(int(splited[5]),user_counter)
    if user == user_counter:
        user_counter+=1
    document = dict()
    document['words'] = dict()
    doc_len = 0
    for i in range(8, 8+length):
        word_idx, freq = splited[i].split(':')
        document['words'][int(word_idx)] = int(freq)
        doc_len += int(freq)
    document['length'] = doc_len
    start = timeit.default_timer()
    my_model.add_event(time, user, document)
    # print(timeit.default_timer()-start)
    if loop_counter % 20 is 0:
        my_model.resample_particles()
    if loop_counter % 50 is 0:
        alpha = my_model.alpha_expectation(time)
