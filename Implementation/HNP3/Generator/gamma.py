import scipy.stats as sst
import math as mth
#  Generating Gamma random variables as sum of exponential variables


#  Only works if alpha parameter is an integer
def gamma_int(n, b):
    sum = 0
    for i in range(0, n):
        u = sst.uniform.rvs(0, 1, 1)
        sum += (-1/b) * mth.log(u)
    return sum


#  General
#  Generating using accept & reject concept
def gamma(a, b):
    a_int = int(mth.modf(a)[1])
    if a_int > 1:
        b_int = b * (a_int - 1) / (a - 1)
        k = (((a - 1) / b) ** (a - a_int)) * mth.exp(a_int - a)
    else:
        b_int = b * (a_int / a)
        k = ((a / b) ** (a - 1)) * mth.exp(1 - a)
    flag = 0
    while flag == 0:
        samp = gamma_int(a_int, b)
        u = sst.uniform.rvs(0, 1, 1)
        p_on_q = (samp ** (a - a_int)) * mth.exp((b_int - b) * samp) / k
        if p_on_q >= u:
            flag = 1
    return samp
for i in range(0, 100):
    print(gamma(100.52, 2.5))

