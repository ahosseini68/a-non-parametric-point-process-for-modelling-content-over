import scipy.stats as sst
import numpy as np
import math as mth


# returns an array proportional to input that sum of it's elements is one
def normalize_sum(v):
    norm = np.sum(v)
    if norm == 0:
        return v
    return v / norm


# returns [min_index, min_value]
def minimum(ls):
    res = [-1, float('inf')]
    for i in range(0, len(ls)):
        if ls[i] < res[1]:
            res[1] = ls[i]
            res[0] = i
    return res


# Hyperparameters
n = int(input('Number Of users: '))
a = float(input('A: '))
b = float(input('B: '))
c = float(input('C: '))
d = float(input('D: '))
vocab_size = 100
topic_size = 100

# topics
H_param = sst.uniform.rvs(0, 1, vocab_size) + 1
phi = sst.dirichlet.rvs(H_param, topic_size)

# alpha & mu
alpha = np.zeros((n, n))
for i in range(0, n):
    alpha[i] = sst.gamma.rvs(c, 0, 1 / d, n)
mu = sst.gamma.rvs(a, 0, 1 / b, n)

# Hawkes process
K = 100  # number of events
events = []
lambdas = [mu]  # intensities
ts = [0]  # t_k s
beta = 0.001
for i in range(1, K + 1):
    ss = []  # s_k s
    for j in range(0, n):
        if lambdas[i - 1][j] == mu[j]:  # the user's intensity has not changed yet
            u = sst.uniform.rvs(0, 1, 1)
            s = -(1 / mu[j]) * mth.log(u)
        else:
            u_1 = sst.uniform.rvs(0, 1, 1)
            u_2 = sst.uniform.rvs(0, 1, 1)
            d = 1 + (beta * mth.log(u_1)) / (lambdas[i - 1][j] - mu[j])
            s2 = -(1 / mu[j]) * mth.log(u_2)
            if d <= 0:
                s = s2
            else:
                s = min(s2, -(1 / beta) * mth.log(d))
        ss.append(s)
    [ind, sk] = minimum(ss)
    ts.append(ts[len(ts) - 1] + sk)
    events.append([ts[len(ts) - 1], ind, []])
    newlambdas = []
    for j in range(0, n):
        newlambdas.append((lambdas[i - 1][j] - mu[j]) * mth.exp(-beta * (ts[i] - ts[i - 1])) + mu[j] + alpha[ind][j])
    lambdas.append(newlambdas)

# Triggering events
triggerer = []
endegenous = []
for i in range(0, K):
    u = events[i][1]
    t = events[i][0]
    sums = mu[u]
    for k in range(0, i):
        sums += alpha[events[k][1]][u] * mth.exp(-beta * (t - events[k][0]))
    u_1 = sst.uniform.rvs(0, sums, 1)
    sums = 0
    flag = 0
    for k in range(0, i):
        sums += alpha[events[k][1]][u] * mth.exp(-beta * (t - events[k][0]))
        if u_1[0] <= sums:
            flag = 1
            triggerer.append(k)
            break
    if flag == 0:
        triggerer.append(i)
        endegenous.append(i)

# Topic indexes
z = np.zeros(K)
gam = 1
for i in range(0, K):
    last_index = -1;
    if triggerer[i] != i:
        z[i] = z[triggerer[i]]
    else:
        m_0 = 0
        t = events[i][0]
        for k in range(0, len(endegenous)):
            m_0 += mth.exp(-(t - events[endegenous[k]][0]))
        u_1 = sst.uniform.rvs(0, m_0 + gam, 1)
        m_0 = 0
        flag = 0
        for k in range(0, len(endegenous)):
            m_0 += mth.exp(-(t - events[endegenous[k]][0]))
            if u_1[0] <= m_0:
                flag = 1
                z[i] = z[k]
                break
        if flag == 0:
            z[i] = last_index + 1
            last_index += 1

# Document Generation
D = 1000  # Document Size
for i in range(0, K):
   doc = []
   u_1 = sst.uniform.rvs(0, 1, D)
   for j in range(0, D):
       sums = 0
       for k in range(0, vocab_size):
           sums = sums + phi[z[i]][k]
           if u_1[j] <= sums:
               doc.append(k)
   events[i][2] = doc

# finished
