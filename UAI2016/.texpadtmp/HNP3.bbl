\begin{thebibliography}{16}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Ahmed and Xing(2012)]{ahmed2012timeline}
Amr Ahmed and Eric~P Xing.
\newblock Timeline: A dynamic hierarchical dirichlet process model for
  recovering birth/death and evolution of topics in text stream.
\newblock \emph{arXiv preprint arXiv:1203.3463}, 2012.

\bibitem[Ahmed et~al.(2011)Ahmed, Ho, Teo, Eisenstein, Xing, and
  Smola]{AhmedOnlineInference}
Amr Ahmed, Qirong Ho, Choon~H Teo, Jacob Eisenstein, Eric~P Xing, and Alex~J
  Smola.
\newblock Online inference for the infinite topic-cluster model: Storylines
  from streaming text.
\newblock In \emph{International Conference on Artificial Intelligence and
  Statistics}, pages 101--109, 2011.

\bibitem[Blei and Frazier(2011)]{blei2011distance}
David~M Blei and Peter~I Frazier.
\newblock Distance dependent chinese restaurant processes.
\newblock \emph{The Journal of Machine Learning Research}, 12:\penalty0
  2461--2488, 2011.

\bibitem[Du et~al.(2012)Du, Song, Yuan, and Smola]{du2012learning}
Nan Du, Le~Song, Ming Yuan, and Alex~J Smola.
\newblock Learning networks of heterogeneous influence.
\newblock In \emph{Advances in Neural Information Processing Systems}, pages
  2780--2788, 2012.

\bibitem[Du et~al.(2013)Du, Song, Woo, and Zha]{UncoverTopicSensitive}
Nan Du, Le~Song, Hyenkyun Woo, and Hongyuan Zha.
\newblock Uncover topic-sensitive information diffusion networks.
\newblock In \emph{Proceedings of the Sixteenth International Conference on
  Artificial Intelligence and Statistics}, pages 229--237, 2013.

\bibitem[Du et~al.(2015)Du, Farajtabar, Ahmed, Smola, and
  Song]{DirichletHawkes}
Nan Du, Mehrdad Farajtabar, Amr Ahmed, Alexander~J Smola, and Le~Song.
\newblock Dirichlet-hawkes processes with applications to clustering
  continuous-time document streams.
\newblock 2015.

\bibitem[Farajtabar et~al.(2014)Farajtabar, Du, Gomez-Rodriguez, Valera, Zha,
  and Song]{farajtabar2014}
Mehrdad Farajtabar, Nan Du, Manuel Gomez-Rodriguez, Isabel Valera, Hongyuan
  Zha, and Le~Song.
\newblock Shaping social activity by incentivizing users.
\newblock In \emph{Advances in neural information processing systems}, pages
  2474--2482, 2014.

\bibitem[Foti et~al.(2015)Foti, Williamson, et~al.]{DependentNPModelsSurvey}
Nicholas~J Foti, Sinead Williamson, et~al.
\newblock A survey of non-exchangeable priors for bayesian nonparametric
  models.
\newblock \emph{Pattern Analysis and Machine Intelligence, IEEE Transactions
  on}, 37\penalty0 (2):\penalty0 359--371, 2015.

\bibitem[He et~al.()He, Rekatsinas, EDU, Foulds, Getoor, Liu, and
  EDU]{HawkesTopic}
Xinran He, EDU~Theodoros Rekatsinas, UMD EDU, James Foulds, EDU~Lise Getoor,
  Yan Liu, and USC EDU.
\newblock Hawkestopic: A joint model for network inference and topic modeling
  from text-based cascades.

\bibitem[Iwata et~al.(2013)Iwata, Shah, and Ghahramani]{Iwata2013}
Tomoharu Iwata, Amar Shah, and Zoubin Ghahramani.
\newblock Discovering latent influence in online social activities via shared
  cascade poisson processes.
\newblock In \emph{Proceedings of the 19th ACM SIGKDD international conference
  on Knowledge discovery and data mining}, pages 266--274. ACM, 2013.

\bibitem[Liniger(2009)]{liniger2009multivariate}
Thomas~Josef Liniger.
\newblock \emph{Multivariate hawkes processes}.
\newblock PhD thesis, Diss., Eidgen{\"o}ssische Technische Hochschule ETH
  Z{\"u}rich, Nr. 18403, 2009, 2009.

\bibitem[Orbanz and Teh(2011)]{orbanz2011bayesian}
Peter Orbanz and Yee~Whye Teh.
\newblock Bayesian nonparametric models.
\newblock In \emph{Encyclopedia of Machine Learning}, pages 81--89. Springer
  US, 2011.

\bibitem[Teh et~al.(2006)Teh, Jordan, Beal, and Blei]{HDP}
Yee~Whye Teh, Michael~I Jordan, Matthew~J Beal, and David~M Blei.
\newblock Hierarchical dirichlet processes.
\newblock \emph{Journal of the american statistical association}, 101\penalty0
  (476), 2006.

\bibitem[Tran et~al.(2015)Tran, Farajtabar, Song, and Zha]{tran2015netcodec}
Long Tran, Mehrdad Farajtabar, Le~Song, and Hongyuan Zha.
\newblock Netcodec: Community detection from individual activities.
\newblock In \emph{SIAM International Conference on Data Mining (SDM)}. SIAM,
  2015.

\bibitem[Yang and Zha(2013)]{ViralDiffusion}
Shuang~Hong Yang and Hongyuan Zha.
\newblock Mixture of mutually exciting processes for viral diffusion.
\newblock In \emph{Proceedings of the 30th International Conference on Machine
  Learning (ICML'13)}, pages 1--9, 2013.

\bibitem[Zhou et~al.(2013)Zhou, Zha, and Song]{Zhou2013a}
Ke~Zhou, Hongyuan Zha, and Le~Song.
\newblock Learning social infectivity in sparse low-rank networks using
  multi-dimensional hawkes processes.
\newblock In \emph{Proceedings of the Sixteenth International Conference on
  Artificial Intelligence and Statistics (AISTAT'13)}, pages 641--649, 2013.

\end{thebibliography}
